USE SAE_caroponsiani

create table CURSOS(
CUR_ID integer NOT NULL PRIMARY KEY(MAT_ID) IDENTITY,
CUR_ANIO INTEGER NOT NULL(CUR_ANIO),
CUR_DESCRIPCION VARCHAR(50)
);

create table MATERIAS(
MAT_ID integer NOT NULL PRIMARY KEY(MAT_ID) IDENTITY,
CUR_ID INTEGER FOREIGN KEY REFERENCES CURSOS(CUR_ID),
MAT_NOMBRE VARCHAR(50)
);
