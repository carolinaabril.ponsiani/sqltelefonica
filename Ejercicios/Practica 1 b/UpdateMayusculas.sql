use Cine_caroponsiani

update generos
set GEN_DESCRIPTION='Terror'
where GEN_ID=1;

update generos
set GEN_DESCRIPTION='Drama'
where GEN_ID=2;

update generos
set GEN_DESCRIPTION='Accion'
where GEN_ID=3;

update generos
set GEN_DESCRIPTION='Documental'
where GEN_ID=4;

update generos
set GEN_DESCRIPTION='Comedia'
where GEN_ID=1;

update peliculas
set PEL_DESCRIPTION= 'La pistola desnuda'
where PEL_ID=1;

update peliculas
set PEL_DESCRIPTION= 'Gladiador'
where PEL_ID=2;

update peliculas
set PEL_DESCRIPTION= 'Titanic'
where PEL_ID=3;

update peliculas
set PEL_DESCRIPTION= 'Rambito y Rambon'
where PEL_ID=4;

update peliculas
set PEL_DESCRIPTION= 'La llamada'
where PEL_ID=5;

update peliculas
set PEL_DESCRIPTION= 'La profecia'
where PEL_ID=6;

update peliculas
set PEL_DESCRIPTION= 'Los bañeros mas locos del mundo'
where PEL_ID=7;
