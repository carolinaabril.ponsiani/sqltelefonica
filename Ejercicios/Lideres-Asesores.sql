USE cine;

drop table lideres;
drop table asesores;

create table lideres(
LID_ID integer NOT NULL PRIMARY KEY(LID_ID) IDENTITY,
LID_DESCRIPTION VARCHAR(50)
);

create table asesores(
ASE_ID integer NOT NULL PRIMARY KEY(ASE_ID) IDENTITY,
LID_ID integer FOREIGN KEY REFERENCES lideres(LID_ID),
ASE_DESCRIPTION VARCHAR(50)
);
